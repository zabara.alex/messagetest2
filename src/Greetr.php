<?php

namespace ZabaraIndustry\Message;

class Greetr
{
    public function getGreet(string $name): string
    {
        return 'Hi ' . $name . '! Welcome to my first package.';
    }
}
